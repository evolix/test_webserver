Il s'agit d'un ensemble de scripts, de type "tests unitaires" permettant de valider le comportement d'un service web, du point de vue d'un client web.

# Installation

Au minimum il faut Ruby pour l'exécution des scripts.

Il faut également :

- OpenSSL
- Python 2
- Bash (>= 4)
- coreutils

Une fois Ruby installé, depuis le dossier du projet :

    bundle install

Ça va installer localement des paquets Ruby nécessaires.

# Exécution

## Partie "sécurité" :

    bundle exec ruby -Itest test/secrity_test.rb

Il est possible d'utiliser un certificat SSL racine stocké en local (`test/certs/_my_cert.pem`) et paramétré dans la méthode `root_certificate`.

Il est possible d'utiliser un binaire `open_ssl` personnalisé, géré dans la méthode `openssl_path`.

`test_certificate_level`

Permet de vérifier la conformité avec un des niveaux normalisés proposés par Mozilla (https://wiki.mozilla.org/Security/Server_Side_TLS)

`test_certificate`

Vérifie la validité du certificat

`test_accepts_tls_v1`

Vérifie que le serveur accepte bien des connexions TLS v1

`test_refuse_ssl_v3`

Vérifie que le serveur refuse bien des connexions SSL v3

`test_hsts_header`

Vérifie la présence de l'en-tête HTTP `HTTP-Strict-Transport-Security`.

## Partie "redirections" :

    bundle exec ruby -Itest test/domain_redirects_test.rb

`test_redirections`

Vérifie que les redirections attendues aient bien lieu.

`test_http_codes`

Vérifie que les codes HTTP attendus soient bien reçus.

## Partie "assets accessibility"

    bundle exec ruby -Itest test/assets_accessibility_test.rb

`test_rss_feeds`

Vérifie la présence de flux RSS sur la page et leur fonctionnement.

`test_head_stylelsheets`

Vérifie la présence de CSS, qu'elles sont bien accessibles, et que leurs en-têtes HTTP les rendent cachables.

`test_head_scripts`

Vérifie la présence de JS, qu'ils sont bien accessibles, et que leurs en-têtes HTTP les rendent cachables.

`test_images`

Vérifie que toutes images de la page sont bien accessibles et cachables.

`test_https_src`

Vérifie que toutes les balises avec attribut `src` sont bien en HTTPS.

`test_cors`

Vérifie les en-têtes CORS sur la feuille de style et toutes les polices web qu'elle référence.

## Partie "cookies"

    bundle exec ruby -Itest test/cookies_test.rb

`test_no_session_on_pages`

Vérifie que certaines pages ne poussent pas de cookies.

`test_session_on_pages`

Vérifie que certaines pages poussent bien un cookie.

## Partie "cache HTTP"

    bundle exec ruby -Itest test/http_cache_test.rb

`test_varnish_hit`

Vérifie que certaines pages sont bien servies puis mises en cache par Varnish.

`test_homepage_first_visit`

Vérifie que certaines pages ont des en-têtes corrects pour le cache public.

`test_homepage_second_visit`

Vérifie que certaines pages rechargées en transmettant les infos de cache reçoivent une réponse "304".

`test_login_first_visit`

Vérifie que certaines pages ne soient pas mises en cache.
