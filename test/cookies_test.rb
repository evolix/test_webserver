require 'test_helper'
require 'mechanize'

class CookiesTest < Minitest::Test
  include WebserverHelper

  def setup
    @agent = Mechanize.new { |a|
      a.follow_redirect = false
    }
  end

  def test_no_session_on_pages
    [
      "/",
      "/faq",
    ].each do |path|
      url = "https://#{domain}#{path}"
      @agent.get(url)

      refute_has_session_cookie @agent, "_example_session", "for #{url}"
    end
  end

  def test_session_on_pages
    [
      "/login",
      "/store",
    ].each do |path|
      url = "https://#{domain}#{path}"
      @agent.get(url)

      assert_has_session_cookie @agent, "_example_session", "for #{url}"
    end
  end

  def assert_has_session_cookie(agent, cookie_name, context = nil)
    message = message_with_context("Expected to find a session cookie named '#{cookie_name}'", context)
    assert_includes agent.cookie_jar.map(&:name), cookie_name, message
  end

  def refute_has_session_cookie(agent, cookie_name, context = nil)
    message = message_with_context("Expected to not find a session cookie named '#{cookie_name}'", context)
    refute_includes agent.cookie_jar.map(&:name), cookie_name, message
  end

end
