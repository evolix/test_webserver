require 'test_helper'
require "mechanize"
require "nokogiri"

class AssetsAccessibilityTest < Minitest::Test
  include WebserverHelper

  def test_rss_feeds
    on_home_page do |page, doc|
      feeds = doc.search("//head/link[@type='application/rss+xml']")

      # Il y a au moins 1 flux RSS
      assert feeds.size >= 1, "Expected to find an RSS feed"

      feeds.each do |feed|
        url = feed["href"]
        assert_status_ok agent.get(url), "for #{url}"
      end
    end
  end

  def test_head_stylelsheets
    on_home_page do |page, doc|
      stylesheets = doc.search("//head/link[@rel='stylesheet']")

      # Il y a au moins 1 CSS "application"
      assert stylesheets.any? { |stylesheet|
        path = URI.parse(stylesheet["href"]).path
        %r(\A/assets/application-\w+\.css\Z) =~ path
      }, "Expected to find the application CSS"

      # Tous les CSS sont accessibles et correctement configurés
      stylesheets.each do |stylesheet|
        url = stylesheet["href"]
        if internal_url?(url)
          assert_cachable_asset agent.get(url), "for #{url}"
        end
      end
    end
  end

  def test_head_scripts
    on_home_page do |page, doc|
      scripts = doc.search("//head/script[@src]")

      # Il y a au moins 1 JS "application"
      assert scripts.any? { |script|
        path = URI.parse(script["src"]).path
        %r|\A/assets/application-\w+\.js\Z| =~ path
      }, "Expected to find the application JS"

      # Tous les scripts externes sont accessibles
      scripts.each do |script|
        url = script["src"]
        if internal_url?(url)
          assert_cachable_asset agent.get(url), "for #{url}"
        end
      end
    end
  end

  def test_images
    on_home_page do |page, doc|
      doc.search("body img").each do |image|
        url = image["src"]
        if internal_url?(url)
          assert_cachable_asset agent.get(url), "for #{url}"
        end
      end
    end
  end

  def test_https_src
    on_home_page do |page, doc|
      doc.search("[src]").each do |element|
        assert_scheme "https", element["src"], "on page #{page.uri}"
      end
    end
  end

  def test_cors
    on_home_page do |page, doc|
      stylesheets = doc.search("//head/link[@rel='stylesheet']")

      # On récupère la feuille de style qui contient les appels aux polices web
      vendor_stylesheet = stylesheets.detect { |stylesheet|
        path = URI.parse(stylesheet["href"]).path
        %r(\A/assets/vendor_bootstrap-\w+\.css\Z) =~ path
      }
      refute_nil vendor_stylesheet, "Expected to find a \"vendor_bootstrap\" CSS file"

      # On extrait toutes les URL de polices web
      css_url = vendor_stylesheet["href"]
      pattern = /url\((?:"|')?([^\)"']+)(?:"|')?\)/
      font_urls = agent.get(css_url).body.scan(pattern).flatten

      refute font_urls.empty?, "Expected to find webfonts in #{css_url}"

      # On vérifie que les en-têtes sont présents
      origin = "https://www.example.com/"
      font_urls.each do |font_url|
        page = agent.get(font_url, [], nil, { "Origin" => origin })
        actual = header(page, "access-control-allow-origin")

        assert_equal origin, actual, "Expected CORS header to match the request origin for #{font_url}"
      end
    end
  end

end
