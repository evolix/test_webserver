source "https://rubygems.org"

gem "test-unit"
gem "mechanize"
gem "nokogiri"
gem "minitest"
gem "minitest-reporters"
